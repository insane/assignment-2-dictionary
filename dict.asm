%include "lib.inc"

section .text
global find_word

find_word:
    .loop:
        cmp rsi, 0
        je .not_exist

        push rdi
        push rsi

        add rsi, 8
        call string_equals
        
        pop rsi
        pop rdi
    
        cmp rax, 0
        jne .exist

        mov rsi, [rsi]
        jmp .loop
    .exist:
        mov rax, rsi
        ret
    .not_exist:
        mov rax, 0
        ret
