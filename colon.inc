%define nxt_key 0
%macro colon 2

%2:
dq nxt_key
db %1, 0
%define nxt_key %2

%endmacro