%include "lib.inc"
%include "words.inc"
%define buffer_size 256

section .data

section .bss
buffer: times buffer_size db 0

section .rodata
buffer_overflow_msg: db "Buffer Overflow", 0
key_not_exist_msg: db "Element with given key does not exist", 0

section .text
global _start
extern find_word

read_line:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r8, rdi
    mov r9, rsi
    .skip_space:
        call read_char
        cmp rax, 0x20
        je .skip_space
        cmp rax, 0x09
        je .skip_space
        cmp rax, 0xa
        je .skip_space
    .loop:
        cmp rax, 0
        je .done
        cmp rax, 0xa
        je .done
        cmp r9, r10
        je .error
        mov byte[r8+r10], al
        inc r10
        call read_char
        jmp .loop
    .done:
        mov byte[r8+r10], 0
        mov rax, r8
        mov rdx, r10
        ret
    .error:
        mov rax, 0
        ret 

    _start:
        mov rdi, buffer
        mov rsi, buffer_size
        call read_line
        cmp rax, 0
        je .buffer_overflow

        mov rdi, buffer
        mov rsi, nxt_key
        call find_word
        
        cmp rax, 0
        je .key_not_exist

        mov rdi, rax
        add rdi, 8
	    push rdi
	    call string_length
	    pop rdi
	    add rax, rdi
	    inc rax

	    mov rdi, rax
	    call print_string
        call exit


    .buffer_overflow:
        mov rdi, buffer_overflow_msg
        call printErr
    
    .key_not_exist:
        mov rdi, key_not_exist_msg
        call printErr