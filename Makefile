ASM=nasm
ASMFLAGS=-f elf64
LD=ld


.PHONY: clean 

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

run: main.o dict.o lib.o
	$(LD) -o $@ $^ 

clean:
	rm *.o run